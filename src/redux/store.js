import { createStore } from "redux"

const initialState = {
   products: [
      {
         id: 1,
         name: "Nike SB Dunk High Space Jam",
         image: "https://drifters.com.ar/uploads/ad/231/dunk-high-space-secundario.jpg",
         price: 2000
      },
      {
         id: 2,
         name: "NIKE SB ALLEYOOP",
         image: "https://drifters.com.ar/uploads/ad/230/vBPfPP5w.jpg",
         price: 4200
      },
      {
         id: 3,
         name: "Soldier Jacket",
         image: "https://drifters.com.ar/uploads/ad/227/slug-CI2655_368_A_PREM.jpg",
         price: 3000
      },
   ],
   cart: {
      products: [],
      totalProducts: 0,
      totalPrice: 0
   }
}

const reducerProduct = (state = initialState, action) => {

   if (action.type === "ADD_PRODUCT_TO_CART") {
      console.log('action', action)
      return {
         ...state,
         cart: {
            ...state.cart,
            products: state.cart.products.concat(action.product),
            totalProducts: state.cart.totalProducts + 1,
            totalPrice: state.cart.totalPrice + action.product.price
         }
      }
   }

   if (action.type === "REMOVE_PRODUCT_FROM_CART") {
      return {
         ...state,
         cart: {
            ...state.cart,
            products: state.cart.products.filter(x => x !== action.product),
            totalProducts: state.cart.totalProducts - 1,
            totalPrice: state.cart.totalPrice - action.product.price
         }
      }
   }


   return state
}

export default createStore(reducerProduct)
