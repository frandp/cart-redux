import React from 'react'

import PropTypes from 'prop-types'
import './SquareButton.scss'

const SquareButton = (props) => {

   const { style, onClick, text } = props

   return (
      <button style={style} onClick={() => onClick()}>{text}</button>
   )
}

SquareButton.propTypes = {
   style: PropTypes.any,
   text: PropTypes.string,
   onClick: PropTypes.func
}

export default SquareButton
