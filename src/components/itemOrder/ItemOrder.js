import React from 'react'

import PropTypes from 'prop-types'
import './ItemOrder.scss'
import { DeleteOutlined } from '@ant-design/icons';
import { useDispatch } from 'react-redux';

const ItemOrder = ({ style, item, name, price, image }) => {

   const dispatch = useDispatch()

   return (
      <div className={'item-container'} style={style}>
         <img alt={'product'} src={image}/>
         <div className={'info-container'}>
            <div>
               <span style={{ fontWeight: 'bold' }}>{name}</span>
            </div>
            <div className={'right-container'}>
               <span>${price}</span>
               <button
                  onClick={() => dispatch({
                     type: "REMOVE_PRODUCT_FROM_CART",
                     product: item
                  })}
               >
                  <DeleteOutlined className={'delete-icon'}/>
               </button>
            </div>
         </div>
      </div>
   )
}

ItemOrder.propTypes = {
   style: PropTypes.any
}

export default ItemOrder
