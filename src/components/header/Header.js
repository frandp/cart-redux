import React from 'react'

import PropTypes from 'prop-types'
import { ShoppingCartOutlined } from '@ant-design/icons';
import './Header.scss'
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

const Header = ({ style }) => {

   const totalProducts = useSelector(state => state.cart.totalProducts)

   return (
      <header style={style} className={'header'}>
         <div className={'middle-line'}/>
         <Link to={`/`}>
            <h2>MY CART</h2>
         </Link>

         <Link to={`/cart`}>
            <ShoppingCartOutlined className={'shop-icon'}/>
            <span className={'total-products-text'}>{totalProducts}</span>
         </Link>
      </header>
   )
}

Header.propTypes = {
   style: PropTypes.any
}



export default Header
