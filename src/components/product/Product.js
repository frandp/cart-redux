import React from 'react'

import PropTypes from 'prop-types'
import './Product.scss'
import SquareButton from '../squareButton/SquareButton';
import { useDispatch, useSelector } from 'react-redux';
import { CheckCircleOutlined } from '@ant-design/icons';

const Product = (props) => {

   const { style, product, name, price, image } = props

   const dispatch = useDispatch()
   const products = useSelector(state => state.cart.products)

   return (
      <div className={'product'} style={style}>
         <img alt={'img'} src={image}/>
         <h3>{name}</h3>
         {
            products.includes(product) ?
               <CheckCircleOutlined className={'check-icon'}/>
               :
               <SquareButton
                  onClick={() => dispatch({
                     type: "ADD_PRODUCT_TO_CART",
                     product
                  })}
                  text={'ADD TO CART'}
               />
         }

         <span className={'price-text'}>{`$${price}`}</span>
      </div>
   )
}

Product.propTypes = {
   style: PropTypes.any
}

export default Product
