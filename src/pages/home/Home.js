import React from 'react'
import PropTypes from 'prop-types'
import Product from '../../components/product/Product';
import { useSelector } from "react-redux";
import './Home.scss'

const Home = ({ style }) => {

   const products = useSelector(state => state.products)

   function renderProducts() {
      return products.map((x, i) => (
         <Product
            key={i}
            product={x}
            {...x}
         />
      ))
   }

   return (
      <div className={'home'} style={style}>
         {renderProducts()}
      </div>
   )
}

Home.propTypes = {
   style: PropTypes.any
}

export default Home
