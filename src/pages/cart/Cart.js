import React from 'react'

import PropTypes from 'prop-types'
import './Cart.scss'
import { useSelector } from 'react-redux';
import ItemOrder from '../../components/itemOrder/ItemOrder';

const Cart = (props) => {

   const { style } = props

   const products = useSelector(state => state.cart.products)
   const totalPrice = useSelector(state => state.cart.totalPrice)

   function renderItems() {
      return products.map((x, i) => (
         <ItemOrder key={i} item={x} {...x}/>
      ))
   }

   return (
      <div className={'container'} style={style}>
         <div className={'items-container'}>
            <h2>Items en tu pedido</h2>
            {
               renderItems()
            }

         </div>
         <div className={'resume-container'}>
            <h3>Resumen</h3>
            <span>TOTAL: ${totalPrice}</span>
         </div>
      </div>
   )
}

Cart.propTypes = {
   style: PropTypes.any
}

export default Cart
