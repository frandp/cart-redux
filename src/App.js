import React from 'react';
import { Provider } from "react-redux"
import store from "./redux/store"
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from './components/header/Header';
import Home from './pages/home/Home';
import Cart from './pages/cart/Cart';

function App() {
   return (
      <Provider store={store}>
         <Router>
            <main>
               <Header/>
               <Route path="/" exact component={Home}/>
               <Route path="/cart" component={Cart}/>
            </main>
         </Router>
      </Provider>
   );
}

export default App;
